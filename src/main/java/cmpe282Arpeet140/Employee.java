package cmpe282Arpeet140;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.springframework.stereotype.Component;

@Component
@Entity(value="employee")
public class Employee {

    @Id
    int id;
    String firstName;
    String lastName;

    public int getId() {
        return id;
    }

    public void setId(int  id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
