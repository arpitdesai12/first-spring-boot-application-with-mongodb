package cmpe282Arpeet140;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/cmpe282Arpeet140/rest/project")
public class ProjectController {

    @Autowired
    ProjectRepository projectRepository;

    private final Logger LOG = LoggerFactory.getLogger(ProjectController.class);

    @RequestMapping(method=RequestMethod.GET)
    public ResponseEntity<List<Project>> getProject(){
        if(projectRepository.count()==0){
            LOG.info("No Projects in database");
            return new ResponseEntity<List<Project>>(HttpStatus.NOT_FOUND);
        }

        LOG.info("Getting all projects");
        return new ResponseEntity<List<Project>>(projectRepository.findAll(),HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.GET, value = "{id}")
    public ResponseEntity<Project> getProjectwithID(@PathVariable int id){
        if(projectRepository.getProjectById(id)==null){
            LOG.info("No project with id: {} in database", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Project project=projectRepository.getProjectById(id);
        LOG.info("Getting project with id: {} ", id);
        return new ResponseEntity<Project>(project, HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.PUT, value="{id}")
    public ResponseEntity<Project> updateTodo(@PathVariable("id") int id,
                                               @Valid @RequestBody Project project) {
        if(projectRepository.getProjectById(id)==null){
            LOG.info("No project with id: {} in database to be updated", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Project project1 = projectRepository.getProjectById(id);
        if(project1 == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(project.getName()!=null)
            project1.setName(project.getName());
        if(String.valueOf(project.getBudget())!=null)
            project1.setBudget(project.getBudget());
        projectRepository.save(project1);
        LOG.info("Project with id: {} in database updated", id);
        return new ResponseEntity<>( HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Project> create(@RequestBody Project project) {
        int id= project.getId();
        if(projectRepository.getProjectById(id)!=null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        projectRepository.save(project);
        LOG.info("A new project with id: {} added", id);
        final URI location = ServletUriComponentsBuilder
                .fromCurrentServletMapping().path("cmpe282Arpeet140/rest/project/{id}").build()
                .expand(id).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<>(headers,HttpStatus.CREATED);
    }

    @RequestMapping(method=RequestMethod.DELETE, value="{id}")
    public ResponseEntity<Project> delete(@PathVariable int id) {

        if(projectRepository.getProjectById(id)==null){
            LOG.info("No project with id: {} to be deleted in database", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Project project=projectRepository.getProjectById(id);
        projectRepository.delete(project);
        LOG.info("Project with id: {} deleted", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

