package cmpe282Arpeet140;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

@Component
@RepositoryRestResource(collectionResourceRel = "employee", path = "employee")
public interface EmployeeRepository1 extends MongoRepository <Employee, String>{

    Employee getEmployeeById (@Param("name") int id);
}
