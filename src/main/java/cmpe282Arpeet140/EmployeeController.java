package cmpe282Arpeet140;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/cmpe282Arpeet140/rest/employee")
public class EmployeeController {

    @Autowired
    EmployeeRepository1 employeeRepository1;

    private final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);

    @RequestMapping(method=RequestMethod.GET)
    public ResponseEntity<List<Employee>> getEmployee(){
        if(employeeRepository1.count()==0){
            LOG.info("No employees in database");
            return new ResponseEntity<List<Employee>>(HttpStatus.NOT_FOUND);
        }

        LOG.info("Getting all employees");
        return new ResponseEntity<List<Employee>>(employeeRepository1.findAll(),HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.GET, value = "{id}")
    public ResponseEntity<Employee> getEmployeewithID(@PathVariable int id){
        if(employeeRepository1.getEmployeeById(id)==null){
            LOG.info("No employee with id: {} in database", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Employee employee=employeeRepository1.getEmployeeById(id);
        LOG.info("Getting employee with id: {} ", id);
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.PUT, value="{id}")
    public ResponseEntity<Employee> updateTodo(@PathVariable("id") int id,
                                           @Valid @RequestBody Employee employee) {
        if(employeeRepository1.getEmployeeById(id)==null){
            LOG.info("No employee with id: {} in database to update", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Employee employee1 = employeeRepository1.getEmployeeById(id);
        if(employee.getFirstName()!=null)
        employee1.setFirstName(employee.getFirstName());
        if(employee.getLastName()!=null)
        employee1.setLastName(employee.getLastName());
        employeeRepository1.save(employee1);
        LOG.info("Employee with id: {} in database updated", id);
        return new ResponseEntity<>( HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Employee> create(@RequestBody Employee employee) {
        int id= employee.getId();
        if(employeeRepository1.getEmployeeById(id)!=null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        employeeRepository1.save(employee);
        LOG.info("Inserted a new employee with id: {} ", id);
        final URI location = ServletUriComponentsBuilder
                .fromCurrentServletMapping().path("cmpe282Arpeet140/rest/employee/{id}").build()
                .expand(id).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<>(headers,HttpStatus.CREATED);
    }

    @RequestMapping(method=RequestMethod.DELETE, value="{id}")
    public ResponseEntity<Employee> delete(@PathVariable int id) {

        if(employeeRepository1.getEmployeeById(id)==null){
            LOG.info("No employee with id: {} in database to delete", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Employee employee=employeeRepository1.getEmployeeById(id);
        employeeRepository1.delete(employee);
        LOG.info("Employee with id: {} deleted", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    }


